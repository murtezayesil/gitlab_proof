# My OpenPGP key

---

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

Public Key: https://keys.openpgp.org/pks/lookup?op=get&options=mr&search=0xbdc1278c02db1e9167b3bc79e2c51fea22fc2243
